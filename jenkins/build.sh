#!/bin/sh

docker pull jenkins/jenkins:lts-jdk11
docker build -t nubixde/jenkins:lts-jdk11 .
